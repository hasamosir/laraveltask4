<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Product;
use Illuminate\Support\Facades\DB;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'item' => 'Ayam Bakar Sambal Terasi',
                'description' => 'Ayam Geprek Sambal Terasi hidangan istimewa rasa gurih dan krispi dari ayamnya karena menggunakan Palmia margarin serbaguna. Pedas sambal terasi menambah kelezatan rasanya bikin ketagihan.',
                'qty' => '25',
                'price' => '20000',
                'image_url' => null,
                'created_at' => new \DateTime,
                'updated_at' => new \DateTime
            ],
            [
                'item' => 'Ayam Bakar Sambal Buto Ijo',
                'description' => 'Varian ayam geprek yang dipadukan dengan sambal dari cabe ijo.',
                'qty' => '25',
                'price' => '20000',
                'image_url' => null,
                'created_at' => new \DateTime,
                'updated_at' => new \DateTime
            ],
            [
                'item' => 'Ayam Bakar Sambal Matah',
                'description' => 'Kombinasi ayam geprek dengan sambal khas daerah Bali, sedap dan nikmat.',
                'qty' => '25',
                'price' => '20000',
                'image_url' => null,
                'created_at' => new \DateTime,
                'updated_at' => new \DateTime
            ],
            [
                'item' => 'Lemon Tea',
                'description' => 'Hidangan manis dan menyegarkan.',
                'qty' => '25',
                'price' => '12000',
                'image_url' => null,
                'created_at' => new \DateTime,
                'updated_at' => new \DateTime
            ],
            [
                'item' => 'Lychee Tea',
                'description' => 'Aroma yang manis dan lembut saat di seduh.',
                'qty' => '25',
                'price' => '12000',
                'image_url' => null,
                'created_at' => new \DateTime,
                'updated_at' => new \DateTime
            ],
            [
                'item' => 'Jus Jeruk',
                'description' => 'Minuman yang luar biasa untuk orang dengan tekanan darah tinggi atau rendah.',
                'qty' => '25',
                'price' => '12000',
                'image_url' => null,
                'created_at' => new \DateTime,
                'updated_at' => new \DateTime
            ]
        ];

        foreach ($data as $material) {
            $product = new Product();
            $product->item = $material['item'];
            $product->description = $material['description'];
            $product->qty = $material['qty'];
            $product->price = $material['price'];
            $product->image_url = $material['image_url'];
            $product->created_at = $material['created_at'];
            $product->updated_at = $material['updated_at'];
            $product->save();
        }
        // DB::table('products')->insert([
        //     'items' => [
        //         'Ayam Bakar Sambal Terasi',
        //         'Ayam Bakar Sambal Buto Ijo',
        //         'Ayam Bakar Sambal Matah',
        //         'Lemon Tea',
        //         'Lychee Tea',
        //         'Jus Jeruk'
        //     ]
        // ]);

        // $product = new Product();
        // $product->item = "Ayam bakar";
        // $product->description = "Ayam kampung";
        // $product->qty = 10;
        // $product->price = 20000;
        // $product->image_url = null;
        // $product->created_at = new \DateTime;
        // $product->updated_at = new \DateTime;
        // $product->save();
    }
}
