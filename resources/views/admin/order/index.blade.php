@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-12">
            <div class="card">
                <div class="card-header text-white bg-primary">Order List</div>
                <div class="card-body">
                    <table class="table">
                        <thead class="table-dark" style="background-color: #718096;">
                            <tr>
                                <th scope="col">User Name</th>
                                <th scope="col">Order Status</th>
                                <th scope="col">Order Date</th>
                                <th scope="col">Tax Amount</th>
                                <th scope="col">Discount Amount</th>
                                <th scope="col">Amount</th>
                                <th scope="col">Action</th>


                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($orders as $item)
                            @php
                            $item = (object) $item
                            @endphp
                            <tr>
                                <td>{{ $item->user->name }}</td>
                                <td>{{ $item->status}}</td>
                                <td>{{ $item->date }}</td>
                                <td>{{ $item->tax_amount }}</td>
                                <td>{{ $item->discount_amount }}</td>
                                <td>{{ $item->amount }}</td>
                                <td>
                                    <a href="{{ route('admin.order.detail', $item->id )}}" type="button" class="btn btn-danger">Lihat Detail</a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
        </div>
    </div>
</div>
@endsection