@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-12">
            <div class="card">
                <div class="card-header text-white bg-primary">Order Detail</div>
                <div class="card-body">
                    <table class="table">
                        <thead class="table-dark" style="background-color: #718096;">
                            <tr>
                                <th scope="col">Item</th>
                                <th scope="col">Qty</th>
                                <th scope="col"></th>
                                <th scope="col">Price</th>
                                <th scope="col"></th>
                                <th scope="col">Total</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($result as $order )
                            @php
                            $item = (object) $order
                            @endphp
                            <tr>
                                <td>{{ $item->product_name }}</td>
                                <td>{{ $item->qty }}</td>
                                <td></td>
                                <td>{{ $item->price }}</td>
                                <td></td>
                                <td>{{ $item->total }}</td>                          
                            </tr>
                            @endforeach
                            <tr>
                                <td></td>
                                <td></td>
                                <td>Sub Total</td>
                                <td></td>
                                <td>{{ $total }}</td>
                            </tr>
                            <tr>
                                <td></td>
                                <td></td>
                                <td>Tax</td>
                                <td></td>
                                <td>{{ $item->tax_amount }}</td>
                            </tr>
                            <tr>
                                <td></td>
                                <td></td>
                                <td>Discount</td>
                                <td></td>
                                <td>{{ $item->discount_amount }}</td>
                            </tr>
                            <tr>
                                <td></td>
                                <td></td>
                                <td><b>Grand Total</b></td>
                                <td></td>
                                <td><b>{{ $total + ($item->tax_amount) - $item->discount_amount }}</b></td>
                            </tr>
                            <tr>
                                <td></td>
                                <td></td>
                                <td><b>Order Date</b></td>
                                <td></td>
                                <td><b>{{ $item->date }}</b></td>     
                            </tr>
                            <tr>
                                <td></td>
                                <td></td>
                                <td><b>Order Status</b></td>
                                <td></td>
                                <td><b>{{ $item->status }}</b></td>   
                            </tr>
                        </tbody>

                    </table>
                    @if ($item->status !== 'Pesanan Selesai')
                        <a href="{{route('admin.order.status', $orderId )}}" type="button" class="btn btn-danger">Update Status</a>
                    @endif                            
                </div>
        </div>
    </div>
</div>
@endsection