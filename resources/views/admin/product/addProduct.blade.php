@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    Add New Product
                    
                </div>
                <div class="card-body">
                    
                <form action="{{ route('product.store')}}" method="POST">
                    @csrf
                    <div class="mb-3">
                        <label class="form-label">Product Name</label>
                        <input type="text" class="form-control" name="item">
                    </div>
                    <div class="mb-3">
                        <label class="form-label">Product Description</label>
                        <input type="text" class="form-control" name="description">
                    </div>
                    <div class="mb-3">
                        <label class="form-label">Stock</label>
                        <input type="integer" class="form-control" name="qty">
                    </div>
                    <div class="mb-3">
                        <label class="form-label">Unit Price</label>
                        <input type="integer" class="form-control" name="price">
                    </div>
                    <div class="mb-3">
                        <label class="form-label">Image Url</label>
                        <input type="text" class="form-control" name="image_url">
                    </div>
                
                    <button type="submit" class="btn btn-primary">Submit</button>
                </form>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection