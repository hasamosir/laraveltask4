@extends('layouts.app')

@section('content')
<div class="container" id = "user-index">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    Products
                    <a href="{{ route('product.create')}}" type="button" class="btn btn-primary" style="float: right">Add Product</a>
                </div>
                <div class="card-body">
                    <table class="table">
                        <thead>
                            <tr>
                                <th scope="col">No</th>
                                <th scope="col">Product Name</th>
                                <th scope="col">Product Description</th>
                                <th scope="col">Stock</th>
                                <th scope="col">Unit Price</th>
                                <th scope="col">Product Image</th>
                                <th scope="col">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($Products as $item)
                            <tr>
                                <th scope="row">{{ $item->id }}</th>
                                <td>{{ $item->item }}</td>
                                <td>{{ $item->description }}</td>
                                <td>{{ $item->qty }}</td>
                                <td>{{ $item->price }}</td>
                                <td><img src="{{ $item->image_url }}"  width="50" height="50"></td>
                                
                                <td>
                                    <a href="{{ route('product.edit', ['product'=> $item->id]  )}}" type="button" class="btn btn-success">Edit</a>
                                    <a href="{{ route('product.show', $item->id )}}" type="button" class="btn btn-secondary">Show</a>
                                    
                                    <form action="{{ route('product.update', $item->id) }}" method="POST">
                                        @csrf
                                        @method('delete')
                                        <button type="submit" class="btn btn-danger">Delete</button>
                                    </form>
                                </td>
                            </tr>                               
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
