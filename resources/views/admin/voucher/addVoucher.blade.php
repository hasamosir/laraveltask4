@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    Add Voucher
                    
                </div>
                <div class="card-body">
                    
                <form action="{{ route('voucher.store')}}" method="POST">
                    @csrf
                    <div class="mb-3">
                        <label class="form-label">ID</label>
                        <input type="text" class="form-control", name="id">
                    </div>
                    <div class="mb-3">
                        <label class="form-label">Voucher Code</label>
                        <input type="text" class="form-control", name="voucher_code">
                    </div>
                    <div class="mb-3">
                        <label class="form-label">Discount Amount</label>
                        <input type="bigInteger" class="form-control", name="discount_amt">
                    </div>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </form>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection