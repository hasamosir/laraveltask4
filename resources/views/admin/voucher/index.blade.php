@extends('layouts.app')

@section('content')
<div class="container" id = "voucher-index">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    Vouchers
                    <a href="{{ route('voucher.create')}}" type="button" class="btn btn-primary" style="float: right">Add Voucher</a>
                </div>
                <div class="card-body">
                    <table class="table">
                        <thead>
                            <tr>
                                <th scope="col">Id</th>
                                <th scope="col">Voucher Code</th>
                                <th scope="col">Discount Amount</th>
                                <th scope="col">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($vouchers as $item)
                            <tr>
                                <th scope="row">{{ $item->id }}</th>
                                <td>{{ $item->voucher_code}}</td>
                                <td>{{ $item->discount_amt }}</td>
                                
                                <td>
                                    <a href="{{ route('voucher.edit', ['voucher'=> $item->id]  )}}" type="button" class="btn btn-success">Edit</a>
                                    <a href="{{ route('voucher.show', $item->id )}}" type="button" class="btn btn-secondary">Show</a>
                                    
                                    <form action="{{ route('voucher.update', $item->id) }}" method="POST">
                                        @csrf
                                        @method('delete')
                                        <button type="submit" class="btn btn-danger">Delete</button>
                                    </form>
                                </td>
                            </tr>                               
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
