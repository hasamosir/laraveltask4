@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    Edit Voucher
                    
                </div>
                <div class="card-body">
                    
                <form action="{{ route('voucher.update', $vouchers->id )}}" method="POST">
                    @csrf
                    @method('PUT')
                    <div class="mb-3">
                        <label class="form-label">ID</label>
                        <input type="text" class="form-control" name="id" value="{{ $vouchers->id }}">
                    </div>
                    <div class="mb-3">
                        <label class="form-label">Voucher Code</label>
                        <input type="text" class="form-control" name="voucher_code" value="{{ $vouchers->voucher_code }}">
                    </div>
                    <div class="mb-3">
                        <label class="form-label">Discount Amount</label>
                        <input type="bigInteger" class="form-control" name="discount_amt" value="{{ $vouchers->discount_amt }}">
                    </div>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </form>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
