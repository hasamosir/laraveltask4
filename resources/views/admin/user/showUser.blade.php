@extends('layouts.app')

@section('content')
<div class="container-fluid">
    
    <div class="card shadow mb-4">
        <div class="card header py-3">
            <div class="row">
                <div class="col d-flex align-item-center justify-content-between">
                    <h6 class="m-0 font-weight-bold text-primary">Users</h6>
                <div>
            </div>
        </div>
        <div class="card-body">

            <div class="form-body">
                <div class="row">
                    <div class="col-md-8">
                        <div class="form-group row">
                            <label class="col-md-2 label-control text-left">Name</label>
                            <div class="col-md-10">
                                <p>{{ $Users->name }}</p>
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-body">
                <div class="row">
                    <div class="col-md-8">
                        <div class="form-group row">
                            <label class="col-md-2 label-control text-left">Email</label>
                            <div class="col-md-10">
                            <p>{{ $Users->email }}</p>
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-body">
                <div class="row">
                    <div class="col-md-8">
                        <div class="form-group row">
                            <label class="col-md-2 label-control text-left">Access</label>
                            <div class="col-md-10">
                            <p>{{ $Users->access_id }}</p>
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-body">
                <div class="row">
                    <div class="col-md-8">
                        <div class="form-group row">
                            <label class="col-md-2 label-control text-left"></label>
                            <div class="col-md-10">
                                <a type="btn" class="btn btn-md btn-secondary" href="{{ route('user.index') }}">Back</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
                
        </div>
    </div>
</div>
@endsection


