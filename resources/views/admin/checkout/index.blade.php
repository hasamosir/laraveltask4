@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-12">
            <div class="card">
                <div class="card-header text-white bg-primary">Checkout Summary</div>
                <div class="card-body">
                    <table class="table">
                        <tbody>
                            @php
                            $total = 0;
                            @endphp
                            @foreach ($result['cartDetails'] as $detail)
                            @php
                            $total += $detail->price * $detail->qty;
                            $tax = $total * 10/100;
                            @endphp
                            <tr>
                                <td>{{ $detail->product->item }}</td>
                                <td>{{ $detail->price }}</td>
                                <td>{{ $detail->qty }}</td>
                                <td>{{ $detail->price * $detail->qty }}</td>
                            </tr>
                            @endforeach
                        </tbody>
                        <tbody>
                            <tr>
                                <td></td>
                                <td></td>
                                <td>Total Order</td>
                                <td>{{ $total }}</td>
                            </tr>
                        </tbody>
                        <tbody>
                            <tr>
                                <td></td>
                                <td></td>
                                <td>Tax 10%</td>
                                <td>{{ $tax }}</td>
                            </tr>
                        </tbody>
                        <tbody>
                            <tr>
                                <td>Kode Voucher</td>
                                <td>
                                    <form method="GET" action="https://merbabu-laravel.jadipesan.com/admin/checkout">
                                        <input type="text" name="voucher" value="{{$vouchers->voucher_code ?? 'Tidak ada'}}">
                                        <input type="submit" class="btn btn-sm btn-danger" value="cek voucher">
                                    </form>
                                </td>
                                <td>Potongan Voucher</td>
                                <td>{{$vouchers->discount_amt ?? 0}}</td>
                            </tr>
                        </tbody>
                        <tbody>
                            <tr>
                                <td></td>
                                <td></td>
                                <td>Total Pembayaran</td>
                                <td>{{ $total + $tax - ($vouchers->discount_amt ?? 0) }}</td>
                            </tr>
                        </tbody>
                    </table>
                    <a type="button" href="{{ route('admin.checkout.process')}}" class="btn btn-sm btn-primary" style="float: right">
                        Process
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection