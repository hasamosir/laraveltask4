@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-12">
            <div class="card">
                <div class="card-header text-white bg-primary">Pemesanan telah diterima</div>
                <div class="card-body">
                    <div class="mb-3 row">
                        <label class="col-sm-4 col-form-label">Dengan Total Pembayaran :</label>
                        <div class="col-6">
                            @php
                            $final = $newOrderDetail->order->tax_amount + $newOrderDetail->order->amount - $newOrderDetail->order->discount_amount
                            @endphp

                            <p>{{$final}}</p>
                        </div>
                        <a type="button" href="{{env('APP_DRUPAL_URL')}}" class="btn btn-sm btn-primary" style="float: right">
                            Apakah anda ingin memesan lagi?
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection