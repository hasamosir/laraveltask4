<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id',
        'status',
        'date',
        'tax_amount',
        'voucher_id',
        'discount_amount',
        'amount'
    ];

    public function details()
    {
        return $this->hasMany(OrderDetail::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function vouchers()
    {
        return $this->hasMany(Voucher::class);
    }

    public function product()
    {
        return $this->belongsTo(Product::class);
    }

    // public function cart()
    // {
    //     return $this->hasMany(Cart::class);
    // }
}
