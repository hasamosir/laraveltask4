<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Order;
use App\Models\OrderDetail;
use Illuminate\Support\Facades\DB;
use PDF;

class HistoriesController extends Controller
{
    public function histories()
    {
        $user = auth()->user()->id ?? null;
        // dd($user);
        $histories = Order::select('id', 'date', DB::raw('tax_amount + amount - discount_amount AS total'), 'status')
            ->where('status', '!=', 'pesanan selesai')
            ->where('user_id', $user)
            ->orderBy('id', 'DESC')
            ->get();
        // dd($histories);

        return view('admin.histories.index', compact('histories'));
    }

    public function detailHistories($id)
    {
        $user = auth()->user()->id ?? null;

        $orders = Order::where('id', $id)->first();

        $result = [
            'order' => $orders,
            'orderDetails' => $orders->details()->with('product')->get(),
        ];

        // dd($result);

        return view('admin.histories.detail', compact('result'));
    }

    public function cetakPdf($id)
    {
        $user = auth()->user()->id ?? null;

        $orders = Order::where('id', $id)->first();

        $result = [
            'order' => $orders,
            'orderDetails' => $orders->details()->with('product')->get(),
        ];

        // dd($result);
        $pdf = PDF::loadview('admin.histories.show', ['result' => $result]);
        // dd($pdf);
        return $pdf->download('list.pdf');
    }
}
