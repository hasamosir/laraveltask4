<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Access;
use App\Http\Requests\ValidasiUserRequest;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $hasAccessUsers = $this->checkAccess();

        $users = User::all();
        return view('admin.user.index', compact('users', 'hasAccessUsers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
        $hasAccessUsers = $this->checkAccess();
        $accesses = Access::all();
        return view('admin.user.addUser', compact('accesses'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ValidasiUserRequest $request)
    {
        $validated = $request->validated();
        
        $hasAccessUsers = $this->checkAccess();
        $isExist = User::where('email', $request->email)->first();
        if($isExist)
        {
            echo'Email Sudah Ada';
        }else{
            $model = new User();
            $model->name = $requests->name;
            $model->email = $requests->email;
            $model->password = password_hash("Rahasia", PASSWORD_BCRYPT);
            $model->access_id = $requests->access_id;
            
            if($model->save())
            {
                return redirect(route('user.index'));
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $hasAccessUsers = $this->checkAccess();

        $Users = User::findOrFail($id);
    
        return view('admin.user.showUser', compact('Users'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($user)
    {
        $hasAccessUsers = $this->checkAccess();

        $accesses = Access::all();
        $Users = User::findOrFail($user);
        return view('admin.user.editUser', compact('Users','accesses'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $hasAccessUsers = $this->checkAccess();
        $Users = User::findOrFail($id);

        $Users->name = $request->name;
        $Users->email = $request->email;
        $Users->access_id = $request->access_id;
       
        $Users->save();

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $hasAccessUsers = $this->checkAccess();
        $Users = User::findOrFail($id);
        if($Users->delete()){
            return redirect(route('user.index'));
        }
    }

    private function checkAccess()
    {
        // $userAccess = auth()->user()->access->name ?? null;
        // if($userAccess !== 'Admin')
        // {
        //     abort(404);
        // }

        // return true;
        $user = auth()->user();
        $accessId = $user->access_id;

        $redirect = 'Drupal';
        if($accessId === 1)
        {
            $redirect = 'menu admin';
        }
    }
}
