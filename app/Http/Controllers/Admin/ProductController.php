<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Product;
use App\Models\Voucher;
use App\Models\Access;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       
        $Products = Product::all();
        return view('admin.product.index', compact('Products'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $Products = Product::all();
        return view('admin.product.addProduct');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $isExist = Product::where('item', $request->item)->first();
        if($isExist)
        {
            echo'Product Tersedia';
        }else{
            $model = new Product();
            $model->item = $request->item;
            $model->description = $request->description;
            $model->qty = $request->qty;
            $model->price = $request->price;
            $model->image_url = $request->image_url;
            
            if($model->save())
            {
                return redirect(route('product.index'));
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $hasAccessUsers = $this->checkAccess();

        $Products = Product::findOrFail($id);
    
        return view('admin.product.showProduct', compact('Products'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $hasAccessUsers = $this->checkAccess();

        $accesses = Access::all();
        $Products = Product::findOrFail($id);
    
        return view('admin.product.editProduct', compact('Products'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $hasAccessUsers = $this->checkAccess();
        $Products = Product::findOrFail($id);

        $Products->item = $request->item;
        $Products->description = $request->description;
        $Products->qty = $request->qty;
        $Products->price = $request->price;
        $Products->image_url = $request->image_url;
       
        $Products->save();
        return redirect(route('product.index'));

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $hasAccessUsers = $this->checkAccess();
        $Products = Product::findOrFail($id);
        if($Products->delete()){
            return redirect(route('product.index'));
        }
    }

    private function checkAccess()
    {
        $user = auth()->user();
        $accessId = $user->access_id;

        $redirect = 'Drupal';
        if($accessId === 1)
        {
            $redirect = 'menu admin';
        }
    }
}
