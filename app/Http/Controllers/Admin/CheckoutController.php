<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Cart;
use App\Models\CartDetail;
use App\Models\Order;
use App\Models\OrderDetail;
use App\Models\Product;
use Illuminate\Support\Facades\DB;
use App\Models\Voucher;

class CheckoutController extends Controller
{
    public function checkout(Request $request, $productid)
    {
        $user = auth()->user();
        if (!$user) {
            return redirect(route('login'));
        }


        $product_id = $productid;
        $qty = $request->qty;


        $cart = Cart::where('user_id', $user->id)->first();
        if (!$cart) {
            $cart = new Cart();
            $cart->user_id = $user->id;
        }

        $details = CartDetail::select('product_id', DB::raw('sum(qty) as qty'))->where('cart_id', $cart->id)->where('product_id', $product_id)->groupBy('product_id')->first();
        $detailsQty = 0;
        if (isset($details->qty)) {
            $detailsQty = $details->qty;
        }

        $product = Product::find($product_id);
        if (!$product) {
            return response()->json([
                'status' => 'error',
                'message' => 'not found'
            ]);
        } else {
            if (($qty + $detailsQty) > $product->qty) {
                return response()->json([
                    'status' => 'error',
                    'message' => 'out of stock'
                ]);
            }
        }

        if (!$cart->save()) {
            return response()->json([
                'status' => 'error',
                'message' => 'server error'
            ]);
        }

        $cartDetailsProduct = $cart->details()->where('product_id', $product_id);
        if ($cartDetailsProduct->count() === 0) {
            $newCart = $cart->details()->create([
                'product_id' => $product_id,
                'price' => $product->price,
                'qty' => $qty
            ]);
        } else {
            $newCart = $cartDetailsProduct->first();
            $newCart->qty += $qty;
            $newCart->save();
        }

        return redirect(env('APP_DRUPAL_URL') . '/menu-foods');
    }

    public function show(Request $request)
    {
        $user = auth()->user();
        if (!$user) {
            return redirect(route('login'));
        }
        $cart = Cart::where('user_id', $user->id)->first();

        if (!$cart) {
            return redirect(env('APP_DRUPAL_URL') . '/menu-foods');
        }

        $vouchers = Voucher::where('voucher_code', $request->voucher)->first();
        if ($vouchers) {
            $cart->voucher_id = $vouchers->id;
            $cart->discount_amount = $vouchers->discount_amt;
            $cart->save();
        }


        $result = [
            'cart' => $cart,
            'cartDetails' => $cart->details()->with('product')->get() ?? [],
        ];

        return view('admin.checkout.index', compact('result', 'vouchers'));
    }

    public function processOrder()
    {
        $user = auth()->user();
        if (!$user) {
            return redirect(route('login'));
        }

        $cart = Cart::where('user_id', $user->id)->first();
        if (!$cart) {
            return redirect(env('APP_DRUPAL_URL'));
        }

        $details = $cart->details()->select(DB::raw('sum(price*qty) as price'))->groupBy('cart_id')->first();
        $total = $details->price ?? 0;


        $transaction = new Order();
        $transaction->user_id = $user->id;
        $transaction->date = date('y-m-d');
        $transaction->tax_amount = $total * 10 / 100;
        $transaction->status = 'Menunggu Pembayaran';
        $transaction->voucher_id = $cart->voucher_id;
        $transaction->discount_amount = $cart->discount_amount;
        $transaction->amount = $total;
        $transaction->save();
        // dd($transaction);

        $cartDetails = $cart->details()->get();
        if ($cartDetails) {
            foreach ($cartDetails as $cartDetail) {
                $newOrderDetail = $transaction->details()->create([
                    'product_id' => $cartDetail->product_id,
                    'price' => $cartDetail->price,
                    'qty' => $cartDetail->qty,
                ]);
            }
        }

        $cart->details()->delete();
        $cart->delete();

        return view('admin.checkout.show', compact('newOrderDetail'));
    }
}
