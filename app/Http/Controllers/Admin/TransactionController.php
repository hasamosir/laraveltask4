<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Product;
use App\Models\Order;
use App\Models\OrderDetail;
use Illuminate\Support\Facades\DB;

class TransactionController extends Controller
{
    public function show()
    {
        $user = auth()->user();
        if (!$user) {
            return redirect(route('login'));
        }
        if ($user->access_id === 3) {
            return redirect(env('APP_DRUPAL_URL'));
        }

        $orders = Order::with('user')->get();

        return view('admin.order.index', compact('orders'));
    }

    public function detail($id)
    {

        $user = auth()->user();
        if (!$user) {
            return redirect(route('login'));
        }

        $orders = Order::where('id', $id)->first();
        $orderDetails = $orders->details()->get();

        $result = [];
        /**
         * pake perulangan
         * yg proses php
         */
        $total = 0;

        foreach ($orderDetails as $key => $detail) {
            if (!isset($result[$detail->product_id])) {
                $result[$detail->product_id] = [
                    'product_id' => $detail->product_id,
                    'product_name' => $detail->product->item,
                    'qty' => $detail->qty,
                    'price' => $detail->price,
                    'description' => $detail->description,
                    'total' => $detail->qty * $detail->price,
                    'status' => $orders->status,
                    'tax_amount' => $orders->tax_amount,
                    'discount_amount' => $orders->discount_amount,
                    'date' => $orders->date,
                    'id' => $orders->id
                ];
            } else {
                $result[$detail->product_id]['qty'] += $detail->qty;
                $result[$detail->product_id]['total'] += $detail->qty * $detail->price;
            }
            $total += $result[$detail->product_id]['total'];
        }

        $orderId = $orders->id;

        return view('admin.order.detail', compact('result', 'total', 'orderId'));
    }

    public function changeStatus($id)
    {

        $UpdateStatus = [
            'Menunggu Pembayaran',
            'Pembayaran Berhasil',
            'Pesanan Diproses',
            'Pesanan Diantar',
            'Pesanan Selesai',
        ];

        $orders = Order::find($id);
        $latestStatus = $orders->status;

        $keyOfLatestStatus = null;
        foreach ($UpdateStatus as $key => $item) {
            if ($item === $latestStatus) {
                $keyOfLatestStatus = $key;
            }
        }
        $keyOfLatestStatus += 1;
        if(!isset($UpdateStatus[$keyOfLatestStatus])){
            
            return redirect(route('admin.order.detail', $id));
        } else {
            $orders->status = $UpdateStatus[$keyOfLatestStatus];
            if ($orders->save()) {

                return redirect(route('admin.order.detail', $id));
            }
        }
    }
}
