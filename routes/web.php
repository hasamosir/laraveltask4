<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect(env('APP_URL') . '/login');
});


Auth::routes();


Route::middleware(['auth'])->namespace('Admin')->group(function () {
    Route::get('/admin/home', 'HomeController@index')->name('admin.home.index');
    Route::resource('/admin/user', 'UserController');
    Route::resource('/admin/product', 'ProductController');
    Route::resource('/admin/voucher', 'VoucherController');

    Route::get('/admin/checkout/process', 'CheckoutController@processOrder')->name('admin.checkout.process');
    Route::get('/admin/checkout/{productId}', 'CheckoutController@checkout'); // nanti bantu cara setting dr drupal
    Route::get('/admin/checkout', 'CheckoutController@show')->name('admin.checkout.show');

    Route::get('/admin/histories', 'HistoriesController@histories')->name('admin.histories.show');
    Route::get('/admin/histories/{id}', 'HistoriesController@detailHistories')->name('admin.histories.detail');
    Route::get('/admin/cetakpdf/{id}', 'HistoriesController@cetakPdf')->name('admin.histories.cetak');

    Route::resource('/transaction', 'TransactionController');
    Route::get('/admin/transaction', 'TransactionController@show')->name('admin.transaction.index');

    Route::get('/admin/order', 'TransactionController@show')->name('admin.order.index');
    Route::get('/admin/order/{id}', 'TransactionController@detail')->name('admin.order.detail');
    Route::get('/admin/order/status/{id}', 'TransactionController@changeStatus')->name('admin.order.status');
});
